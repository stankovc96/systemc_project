#include "tb_vp.hpp"

SC_HAS_PROCESS(TB_VP);

TB_VP::TB_VP(sc_module_name name):
	sc_module(name)
{
	SC_THREAD(test);
	 p_port0.bind(sig0);
	 sig0=SC_LOGIC_0;
}

void TB_VP::test()
{
	sc_time loct;
	tlm_generic_payload pl;
	tlm_utils::tlm_quantumkeeper qk;
	qk.reset();
	
	//**********************************************************************
	// Writing data to BRAM_CURR and BRAM_REF
	//**********************************************************************
	SC_REPORT_INFO("TB",string("Writing CURR IMG data to BRAM_CURR...@ " + sc_time_stamp().to_string()).c_str());
	for (int i = 0; i < 16384; i++)
	{
		sc_uint<32> data_curr = pix91[i]; //current frame pixel
		
        pl.set_address(VP_ADDR_BRAM_CTRL_0+i*4); //address 32bit
		pl.set_command(TLM_WRITE_COMMAND);//write
		pl.set_data_length(1);
		pl.set_data_ptr((unsigned char*)&data_curr);//data 32bit
		
        isoc->b_transport(pl, loct);
        
		//qk.set_and_sync(loct);
		//msg(pl);
		loct += sc_time(10, SC_NS);
	/**********************************************************************/
		sc_uint<32> data_ref = pix90[i];//reference frame pixel
		
        pl.set_address(VP_ADDR_BRAM_CTRL_1+i*4); //address 32bit
		pl.set_command(TLM_WRITE_COMMAND);//write
		pl.set_data_length(1);
		pl.set_data_ptr((unsigned char*)&data_ref);//data 32bit
		
        isoc->b_transport(pl, loct);
        
		//qk.set_and_sync(loct);
		//msg(pl);
		loct += sc_time(10, SC_NS);
	}
	qk.set_and_sync(loct);
	SC_REPORT_INFO("TB",string("END writing data to BRAM_CURR and BRAM_REF...@ " + sc_time_stamp().to_string()).c_str());

	//**********************************************************************
	// START=1
	//**********************************************************************
    sc_uint<32> start = 1;
		
    pl.set_address(VP_ADDR_AXI_L_ARPS_START_I); 
    pl.set_command(TLM_WRITE_COMMAND);
    pl.set_data_length(1);
    pl.set_data_ptr((unsigned char*)&start);

    isoc->b_transport(pl, loct);
		
    qk.set_and_sync(loct);
    msg(pl);
    loct += sc_time(10, SC_NS);
	
	//**********************************************************************
	// START=0
	//**********************************************************************
    start = 0;
		
    pl.set_address(VP_ADDR_AXI_L_ARPS_START_I); 
    pl.set_command(TLM_WRITE_COMMAND);
    pl.set_data_length(1);
    pl.set_data_ptr((unsigned char*)&start);

    isoc->b_transport(pl, loct);
		
    qk.set_and_sync(loct);
    msg(pl);
    loct += sc_time(10, SC_NS);
		
	//**********************************************************************
	// Wait for interrupt.
	//**********************************************************************
	
    while( tmp_sig == SC_LOGIC_0){
        tmp_sig=sig0.read();
    }
    
    string folder="output/outputs.txt";
    fstream f;
    f.open(folder,ios::out);
	
    qk.set_and_sync(loct);
    //**********************************************************************
	//Reading MV from BRAM_MV
    //**********************************************************************
    SC_REPORT_INFO("TB",string("Reading MV from BRAM_MV...@ " + sc_time_stamp().to_string()).c_str());
	
    for (int i = 0; i < 512; i++){
        sc_dt::sc_int<32> mv;
        pl.set_address(VP_ADDR_BRAM_CTRL_2+i*4);
        pl.set_command(TLM_READ_COMMAND);
        pl.set_data_length(1);
        pl.set_data_ptr((unsigned char*)&mv);
        isoc->b_transport(pl, loct);
        loct += sc_time(10, SC_NS);
        //msg(pl);
        f<<(mv)<<"\n";//write in txt file outputs
    }
   
    qk.set_and_sync(loct);
    SC_REPORT_INFO("TB",string("END Reading MV from BRAM_MV...@ " + sc_time_stamp().to_string()).c_str());
	
    f.close();
	
    #ifdef COMPARE
        string txt_out="output/outputs.txt";
        string com_out="compare/s90s91.txt";
        fstream f_o;
        fstream f_c;
        string o_data,c_data;
        int o_arr[512];
        int c_arr[512];
        f_o.open(txt_out,ios::out | ios::in);
        f_c.open(com_out,ios::out | ios::in);
        int cnt=0;
        while(getline(f_o,o_data)){
            o_arr[cnt]=std::stoi(o_data);
            cnt++;
        }
        cnt=0;
        while(getline(f_c,c_data)){
            c_arr[cnt]=std::stoi(c_data);
            cnt++;
        }
        char res=1;//everything is ok
        for(int cc=0;cc<512;cc++){
            if(o_arr[cc]!=c_arr[cc]){
                SC_REPORT_INFO("TB",string("MV don't match!!! MV_pos="+to_string(cc)).c_str());
                //std::cout<<cc<<"\n";
                res=0;
            }
        }
        if(res==1){
            SC_REPORT_INFO("TB","Motion Vectors match");		
        }
        f_o.close();
        f_c.close();
    #endif
    /***********************************************************************/
    std::cout<<"\n-----------------------------------------------------------"<<std::endl;
    
    
    //std::cout<<"cnt_arps_write_bus="<<cnt_arps_write_bus<<std::endl;
    //std::cout<<"cnt_arps_read_bus="<<cnt_arps_read_bus<<std::endl;
    std::cout<<"Number of transactions in 'sad_block': "<<cnt_arps_sad_block<<std::endl;
    std::cout<<"Number of transactions in 'control_block': "<<cnt_arps_control_block<<std::endl;
    //std::cout<<"The number of bytes that ARPS module sends: "<<cnt_byte_sent<<" B"<<std::endl;
    //std::cout<<"The number of bytes that ARPS module receives: "<<cnt_byte_received<<" B"<<std::endl;
	
    std::cout<<"\n-----------------------------------------------------------"<<std::endl;
    std::cout<<"BRAM_CURR interface:\n";
    std::cout<<"Number of bytes sent: \t"<<cnt_byte_sent_b_curr<<" B"<<std::endl;
    std::cout<<"Number of bytes received: "<<cnt_byte_rec_b_curr<<" B"<<" Time: "<<time_bram_curr.to_string()<<std::endl;
    std::cout<<"Data flow: "<<(cnt_byte_rec_b_curr/(time_bram_curr.to_double()))*1000000<<"MB/S"<<std::endl;
    std::cout<<std::endl;
    
    std::cout<<"BRAM_REF interface:\n";
    std::cout<<"Number of bytes sent: "<<cnt_byte_sent_b_ref<<" B"<<std::endl;
    std::cout<<"Number of bytes received: "<<cnt_byte_rec_b_ref<<" B"<<" Time: "<<time_bram_ref.to_string()<<std::endl;
    std::cout<<"Data flow: "<<(cnt_byte_rec_b_ref/(time_bram_ref.to_double()))*1000000<<"MB/S"<<std::endl;
    std::cout<<std::endl;
    
    std::cout<<"BRAM_MV interface:\n";
    std::cout<<"Number of bytes received: "<<cnt_byte_rec_b_mv<<" B"<<std::endl;
    std::cout<<"Number of bytes sent:  "<<cnt_byte_sent_b_mv<<" B"<<" Time: "<<time_bram_mv.to_string()<<std::endl;
    std::cout<<"Data flow: "<<(cnt_byte_sent_b_mv/(time_bram_mv.to_double()))*1000000<<"MB/S"<<std::endl;
    
    
    std::cout<<"-----------------------------------------------------------"<<std::endl;
    //**********************************************************************
	// Wait 1 more micro second and finish simulation.
	//**********************************************************************
    qk.inc(sc_time(10, SC_US));
    qk.sync();
    sc_stop();//end
}


void TB_VP::msg(const pl_t& pl)
{
	static int trcnt = 0;
	stringstream ss,val_ss;
	ss << hex << pl.get_address();
	sc_uint<32> val = *((sc_uint<32>*)pl.get_data_ptr());
    val_ss<<hex<<val;
	//string msg = "val: " + to_string((int)val) + " adr: " + ss.str();
    string msg = "val: 0x" + val_ss.str() + " adr: 0x" + ss.str();
	msg += " @ " + sc_time_stamp().to_string();

	SC_REPORT_INFO("TB", msg.c_str());
	trcnt++;
	SC_REPORT_INFO("TB", ("------------" + to_string(trcnt)).c_str());
}
void TB_VP::report_signal(sc_dt::sc_logic L)
{
	string s1="Interrupt=";
	if(L==SC_LOGIC_1){
		s1+=to_string((int)1)+" @ " + sc_time_stamp().to_string();
	}
	else{
		s1+=to_string((int)0)+" @ " + sc_time_stamp().to_string();
	}
	SC_REPORT_INFO("TB",s1.c_str());
}
