#include "interconnect.hpp"

INTERCONNECT::INTERCONNECT(sc_module_name name) :
	sc_module(name)
    , offset(sc_core::SC_ZERO_TIME)
{
	s_cpu.register_b_transport(this, &INTERCONNECT::b_transport);
}

void INTERCONNECT::b_transport(pl_t& pl, sc_core::sc_time& offset)
{
	uint64 addr = pl.get_address();
	uint64 taddr;
	offset += sc_time(10, SC_NS);

	if (addr >= VP_ADDR_AXI_L_ARPS_L && addr <= VP_ADDR_AXI_L_ARPS_H)
	{
		taddr = addr & 0x0000000F;
		pl.set_address(taddr);
		s_arps->b_transport(pl, offset);
		//msg(pl);
	}
    else if(addr >= VP_ADDR_BRAM_CTRL_0_L && addr <= VP_ADDR_BRAM_CTRL_0_H)
    {
        taddr = addr & 0x0000FFFF;
		pl.set_address(taddr);
		s_bram_ctrl_0->b_transport(pl, offset);
    }
    else if(addr >= VP_ADDR_BRAM_CTRL_1_L && addr <= VP_ADDR_BRAM_CTRL_1_H)
    {
        taddr = addr & 0x0000FFFF;
		pl.set_address(taddr);
		s_bram_ctrl_1->b_transport(pl, offset);
    }
    else if(addr >= VP_ADDR_BRAM_CTRL_2_L && addr <= VP_ADDR_BRAM_CTRL_2_H)
    {
        taddr = addr & 0x00000FFF;//<=treba 00000FFF
        //std::cout<<"taddr="<<taddr<<std::endl;
		pl.set_address(taddr);
		s_bram_ctrl_2->b_transport(pl, offset);
    }
	else
    {
      SC_REPORT_ERROR("Interconnect", "Wrong address.");
      pl.set_response_status ( tlm::TLM_ADDRESS_ERROR_RESPONSE );
    }

	pl.set_address(addr);
    offset += sc_core::sc_time(10, sc_core::SC_NS);
}

void INTERCONNECT::msg(const pl_t& pl)
{
	stringstream ss;
	ss << hex << pl.get_address();
	sc_uint<32> val = *((sc_uint<32>*)pl.get_data_ptr());
	string cmd  = pl.get_command() == TLM_READ_COMMAND ? "read  " : "write ";

	string msg = cmd + "val: " + to_string((int)val) + " adr: " + ss.str();
	msg += " @ " + sc_time_stamp().to_string();

	SC_REPORT_INFO("BUS FWD", msg.c_str());
}
