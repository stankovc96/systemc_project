#ifndef _ARPS_H_
#define _ARPS_H_

#include <tlm>
#include <tlm_utils/simple_target_socket.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/tlm_quantumkeeper.h>
#include <sstream>
#include <math.h> 
#include "types.hpp"

#define PERIOD_T sc_core::sc_time(10, SC_NS)

/**********************************************/
extern unsigned int cnt_arps_read_bus;
extern unsigned int cnt_arps_write_bus;
extern unsigned int cnt_arps_sad_block;
extern unsigned int cnt_arps_control_block;

//counters for bram interfaces
extern unsigned int cnt_byte_sent_b_curr;
extern unsigned int cnt_byte_rec_b_curr;
extern unsigned int cnt_byte_sent_b_ref;
extern unsigned int cnt_byte_rec_b_ref;
extern unsigned int cnt_byte_sent_b_mv;
extern unsigned int cnt_byte_rec_b_mv;
//time for bram interfaces
extern sc_core::sc_time time_bram_curr;
extern sc_core::sc_time time_bram_ref;
extern sc_core::sc_time time_bram_mv;


typedef tlm::tlm_base_protocol_types::tlm_payload_type pl_t;

class ARPS : public sc_core::sc_module
{
public:
	ARPS(sc_core::sc_module_name);

	tlm_utils::simple_target_socket    <ARPS> soc;        //TARGET
	tlm_utils::simple_initiator_socket <ARPS> bram_curr_b;//INITIATOR
	tlm_utils::simple_initiator_socket <ARPS> bram_ref_b; //INITIATOR
	tlm_utils::simple_initiator_socket <ARPS> bram_mv_b;  //INITIATOR
	 
	sc_core::sc_out<sc_dt::sc_logic> p_out; //interrupt

	inline void set_period(sc_core::sc_time&);
    
protected:
	
	//AXI_LITE reg
	sc_core::sc_time period;
    
	sc_dt::sc_uint<1> start_i;
	sc_dt::sc_uint<1> ready_o;
	
	/**********************************************/
	sc_core::sc_time offset;
	
	//sad_block
	sc_dt::sc_uint<16> costSAD(	sc_dt::sc_uint<8>,
							    sc_dt::sc_uint<8>,
								sc_dt::sc_uint<8>,
								sc_dt::sc_uint<8>);
	
	char checkBounded(int ,int ,unsigned int ,unsigned int );//method
	
	

	void b_transport(pl_t&, sc_core::sc_time&);
	void proc();//contol_block
	void msg(const pl_t&);
    
    //Method that conv. 16bit addr to 32 bit addr and 32 bit data to 8 bit data
    sc_dt::sc_uint<8> addr_data_resize(sc_dt::sc_uint<16> addr_in,char socket);
    
    //BRAM methods for writing and reading  
	sc_dt::sc_uint<32> read_bram(sc_dt::sc_uint<32> addr, char socket);
    void write_bram(sc_dt::sc_uint<32> addr, sc_dt::sc_int<32> data, char socket);
    
};

void ARPS::set_period(sc_core::sc_time& t) 
{
	period = t;
}

#endif
