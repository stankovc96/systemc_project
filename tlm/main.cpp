#include <systemc>
#include "tb_vp.hpp"
#include "vp.hpp"
#include <sstream>
#define QUANTUM

using namespace sc_core;

int sc_main(int argc, char* argv[])
{
	VP vp("VP");
	TB_VP tb("TB_VP");
	
	tb.isoc.bind(vp.s_cpu);//connect tb =>vp
	vp.p_out0.bind(tb.p_port0);//connect interrupt
	
	tlm::tlm_global_quantum::instance().set(sc_time(10, SC_NS));

	sc_start();

    return 0;
}
