In 'compare' folder are referent results given by Python,C++ and Matlab
In 'output' folder are results given by Virtual Platform 

Counters:
cnt_arps_write_bus=262660
cnt_arps_read_bus=512
cnt_arps_sad_block=376320
cnt_arps_control_block=1470

*cnt_arps_write_bus : number of writes in arps module

*cnt_arps_read_bus : number of reads in arps module

*cnt_arps_sad_block : function which calculates sum of absolut differences of block 16x16

*cnt_arps_control_block : main function that defines the flow of algoritam
