#ifndef _TB_VP_H_
#define _TB_VP_H_

#include <sstream>
#include <fstream>
#include <string>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/tlm_quantumkeeper.h>

#include "arps.hpp"
#include "types.hpp"
#include "sample90.hpp"
#include "sample91.hpp"

#define COMPARE 1

using namespace sc_core;
using namespace sc_dt;
using namespace std;
using namespace tlm;



class TB_VP :
	public sc_core::sc_module
{
public:
	TB_VP(sc_core::sc_module_name);

	tlm_utils::simple_initiator_socket<TB_VP> isoc; //INITIATOR
	sc_out<sc_logic> p_port0;                       //out interrupt
	sc_signal <sc_logic> sig0;                      //signal interrupt

protected:

	void test();

	typedef tlm::tlm_base_protocol_types::tlm_payload_type pl_t;
	void report_signal(sc_dt::sc_logic L);
	void msg(const pl_t&);
	sc_dt::sc_logic tmp_sig;
};


#endif
