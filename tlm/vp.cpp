#include "vp.hpp"


using namespace sc_core;

VP::VP(sc_module_name name) :
	sc_module(name),
	in("interconnect"),
	ar("arps"),
    b_ctrl_0("bram_ctrl_0"),
    b_ctrl_1("bram_ctrl_1"),
    b_ctrl_2("bram_ctrl_2"),
	b_curr("bram_curr",16384),/*16384 location in BRAM*/
	b_ref("bram_ref",16384),
	b_mv("bram_mv",512)
{
	s_cpu.register_b_transport(this, &VP::b_transport);

	s_interconnect.bind(in.s_cpu);                              //cpu=>interconnect
	in.s_arps.bind(ar.soc);                                     //interconnect=>arps
	in.s_bram_ctrl_0.bind(b_ctrl_0.vp_socket);                  //interrconect=>bram_ctrl_0
	in.s_bram_ctrl_1.bind(b_ctrl_1.vp_socket);                  //interrconect=>bram_ctrl_1
	in.s_bram_ctrl_2.bind(b_ctrl_2.vp_socket);                  //interrconect=>bram_ctrl_2
    
    b_ctrl_0.bram_ctrl_port_a_soc.bind(b_curr.bram_port_a_soc); //bram_ctrl_0=>bram_curr_port_a
    b_ctrl_1.bram_ctrl_port_a_soc.bind(b_ref.bram_port_a_soc);  //bram_ctrl_1=>bram_ref_port_a
    b_ctrl_2.bram_ctrl_port_a_soc.bind(b_mv.bram_port_a_soc);   //bram_ctrl_2=>bram_mv_port_a
    
    ar.bram_curr_b.bind(b_curr.bram_port_b_soc);                //arps => bram_curr_port_b
    ar.bram_ref_b.bind(b_ref.bram_port_b_soc);                  //arps => bram_ref_port_b
    ar.bram_mv_b.bind(b_mv.bram_port_b_soc);                    //arps => bram_mv_port_b
	
	ar.p_out(p_out0);                                           //arps interrupt
	SC_REPORT_INFO("VP", "Platform is constructed");
}

void VP::b_transport(pl_t& pl, sc_time& delay)
{
	s_interconnect->b_transport(pl, delay);
	//SC_REPORT_INFO("VP", "Transaction passes...");
}
