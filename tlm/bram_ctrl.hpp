#ifndef BRAM_CTRL_HPP_
#define BRAM_CTRL_HPP_

#include <systemc>
#include <tlm>
#include <tlm_utils/simple_target_socket.h>
#include <tlm_utils/simple_initiator_socket.h>
#include "types.hpp"

class BRAM_CTRL: public sc_core::sc_module
{
public:
  BRAM_CTRL (sc_core::sc_module_name name);
  ~BRAM_CTRL ();
  tlm_utils::simple_target_socket<BRAM_CTRL> vp_socket;//TARGET
  tlm_utils::simple_initiator_socket<BRAM_CTRL> bram_ctrl_port_a_soc;//INITIATOR
  //tlm_utils::simple_initiator_socket<BRAM_CTRL> bram_ctrl_port_b_soc;//INITIATOR

protected:
  void b_transport (pl_t &, sc_core::sc_time &);
  pl_t   pl_bram_port_a;
  //pl_t   pl_bram_port_b;
};

#endif
