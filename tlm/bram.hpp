#ifndef BRAM_HPP
#define BRAM_HPP

#include <systemc>
#include <tlm>
#include <tlm_utils/simple_target_socket.h>
#include <vector>
#include "types.hpp"
#include <sstream>
#include <string>

using namespace std;
using namespace tlm;
using namespace sc_core;
using namespace sc_dt;

class BRAM : public sc_core::sc_module
{
public:
  BRAM (sc_core::sc_module_name name,sc_dt::sc_uint<32> size);
  ~BRAM();
  tlm_utils::simple_target_socket<BRAM> bram_port_a_soc; // TARGET
  tlm_utils::simple_target_socket<BRAM> bram_port_b_soc; //TARGET
protected:
  void b_transport(pl_t &, sc_core::sc_time &);
  std::vector<sc_dt::sc_uint<32>> mem;
  sc_dt::sc_uint<32> MEM_RESERVED;
  	void msg(const pl_t&);
  
};

#endif
