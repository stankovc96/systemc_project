#include "bram.hpp"

BRAM::BRAM(sc_core::sc_module_name name,sc_dt::sc_uint<32> size) : sc_module(name),
                                                                   MEM_RESERVED(size)
{
  bram_port_a_soc.register_b_transport(this, &BRAM::b_transport);
  bram_port_b_soc.register_b_transport(this, &BRAM::b_transport);
  mem.reserve(MEM_RESERVED);
  //init BRAM
  for (int i=0;i<MEM_RESERVED;i++){
    mem.push_back(0);
  }
  //SC_REPORT_INFO("BRAM", "Constructed.");
}

BRAM::~BRAM()
{
  //SC_REPORT_INFO("BRAM", "Destroyed.");
}

void BRAM::b_transport(pl_t &pl, sc_core::sc_time &offset)
{
  tlm::tlm_command cmd = pl.get_command();
  sc_dt::uint64 addr = pl.get_address();
  //unsigned int len = pl.get_data_length();
  unsigned char *buf = pl.get_data_ptr();
  sc_dt::sc_uint<32> taddr = addr>>2;//div 4
  sc_dt::sc_uint<32> tdata=mem[taddr];

	switch(cmd)
    {
    case tlm::TLM_WRITE_COMMAND:
      mem[taddr] = *((sc_dt::sc_uint<32>*)buf);
      pl.set_response_status( tlm::TLM_OK_RESPONSE );
      break;
    case tlm::TLM_READ_COMMAND:
      memcpy(buf,&tdata,sizeof(tdata));
      pl.set_data_ptr((unsigned char*)&buf);
      pl.set_response_status( tlm::TLM_OK_RESPONSE );
      //msg(pl);
      break;
    default:
      pl.set_response_status( tlm::TLM_COMMAND_ERROR_RESPONSE );
    }

  offset += sc_core::sc_time(10, sc_core::SC_NS);
}

void BRAM::msg(const pl_t& pl)
{
	stringstream ss;
	ss << hex << pl.get_address();
	sc_uint<32> val = *((sc_uint<32>*)pl.get_data_ptr());
	string cmd  = pl.get_command() == TLM_READ_COMMAND ? "read  " : "write ";

	string msg = cmd + "val: " + to_string((int)val) + " adr: " + ss.str();
	msg += " @ " + sc_time_stamp().to_string();

	SC_REPORT_INFO("BRAM", msg.c_str());
}


