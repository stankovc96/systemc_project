#ifndef  _INTERCONNECT_HPP_
#define _INTERCONNECT_HPP_

#include <systemc>
#include <tlm>
#include <tlm_utils/simple_target_socket.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <sstream>
#include "types.hpp"
#include <string>

using namespace std;
using namespace tlm;
using namespace sc_core;
using namespace sc_dt;

class INTERCONNECT :
	public sc_core::sc_module
{
public:
	INTERCONNECT(sc_core::sc_module_name);

	tlm_utils::simple_target_socket<INTERCONNECT> s_cpu;//TARGET
	tlm_utils::simple_initiator_socket<INTERCONNECT> s_arps;//INITIATOR
	tlm_utils::simple_initiator_socket<INTERCONNECT> s_bram_ctrl_0;//INITIATOR
	tlm_utils::simple_initiator_socket<INTERCONNECT> s_bram_ctrl_1;//INITIATOR
	tlm_utils::simple_initiator_socket<INTERCONNECT> s_bram_ctrl_2;//INITIATOR

protected:
	typedef tlm::tlm_base_protocol_types::tlm_payload_type pl_t;
    sc_core::sc_time offset;
	void b_transport(pl_t&, sc_core::sc_time&);
	void msg(const pl_t&);
};

#endif
