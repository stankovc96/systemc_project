#include "bram_ctrl.hpp"

BRAM_CTRL::BRAM_CTRL(sc_core::sc_module_name name) : sc_module(name)
{
  vp_socket.register_b_transport(this, &BRAM_CTRL::b_transport);
  //SC_REPORT_INFO("BRAM Controller", "Constructed.");
}

BRAM_CTRL::~BRAM_CTRL()
{
  //SC_REPORT_INFO("BRAM Controller", "Destroyed.");
}

void BRAM_CTRL::b_transport(pl_t &pl, sc_core::sc_time &offset)
{
  tlm::tlm_command cmd = pl.get_command();
  sc_dt::uint64 addr = pl.get_address();
  unsigned int len = pl.get_data_length();
  unsigned char * data = pl.get_data_ptr();
  
  pl_bram_port_a.set_command(cmd);
  pl_bram_port_a.set_address(addr);
  pl_bram_port_a.set_data_length(1);
  pl_bram_port_a.set_data_ptr(data);
  pl_bram_port_a.set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
/*
  pl_bram_port_b.set_command(cmd);
  pl_bram_port_b.set_address(addr);
  pl_bram_port_b.set_data_length(1);
  pl_bram_port_b.set_data_ptr(buf);
  pl_bram_port_b.set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
*/
  bram_ctrl_port_a_soc->b_transport(pl_bram_port_a,offset);
  if (pl_bram_port_a.is_response_error()) 
	  SC_REPORT_ERROR("BRAM_CTRL",pl_bram_port_a.get_response_string().c_str());
/*
  bram_ctrl_port_b_soc->b_transport(pl_bram_port_b,offset);
  if (pl_bram_port_b.is_response_error())
	SC_REPORT_ERROR("BRAM_REF",pl_bram_port_b.get_response_string().c_str());
*/
}
