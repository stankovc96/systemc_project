#include "arps.hpp"

using namespace sc_core;
using namespace sc_dt;
using namespace std;
using namespace tlm;

//counters
unsigned int cnt_arps_read_bus=0;
unsigned int cnt_arps_write_bus=0;
unsigned int cnt_arps_sad_block=0;
unsigned int cnt_arps_control_block=0;

//counters for bram interfaces
unsigned int cnt_byte_sent_b_curr=0;
unsigned int cnt_byte_rec_b_curr=0;
unsigned int cnt_byte_sent_b_ref=0;
unsigned int cnt_byte_rec_b_ref=0;
unsigned int cnt_byte_sent_b_mv=0;
unsigned int cnt_byte_rec_b_mv=0;

//time for bram interfaces
sc_core::sc_time time_bram_curr = sc_core::SC_ZERO_TIME;
sc_core::sc_time time_bram_ref  = sc_core::SC_ZERO_TIME;
sc_core::sc_time time_bram_mv   = sc_core::SC_ZERO_TIME;


SC_HAS_PROCESS(ARPS);

ARPS::ARPS(sc_module_name name):
	sc_module(name),
	soc("soc"),
	period(10, SC_NS),
	start_i(0),
    offset(sc_core::SC_ZERO_TIME)
{
	soc.register_b_transport(this, &ARPS::b_transport);
	SC_THREAD(proc);//proces
}

/**************************************************************************************************/
void ARPS::b_transport(pl_t& pl, sc_time& offset)
{
    tlm_command	   cmd  = pl.get_command();
    uint64         addr = pl.get_address();
    unsigned char* data = pl.get_data_ptr();

	switch(cmd)
	{
	case TLM_WRITE_COMMAND:
	{   
        cnt_arps_write_bus++;
		switch(addr)
		{
		/*-----------------------------------------------------------------------------*/
		case ARPS_START_I:
			start_i = *((sc_uint<1>*)data) ;
			pl.set_response_status( TLM_OK_RESPONSE );
			msg(pl);
			break;
		case ARPS_READY_O:
			//ready_o = *((sc_uint<32>*)data);
			pl.set_response_status( TLM_ADDRESS_ERROR_RESPONSE );
			break;
		
		/*-----------------------------------------------------------------------------*/
		default:
			pl.set_response_status( TLM_ADDRESS_ERROR_RESPONSE );
			break;
		}
		break;
	}
	case TLM_READ_COMMAND:
	{
        cnt_arps_read_bus++;
		switch(addr)
		{
		case ARPS_START_I:
			memcpy(data, &start_i, sizeof(start_i));
			pl.set_response_status( TLM_OK_RESPONSE );
			break;
		case ARPS_READY_O:
			memcpy(data, &ready_o, sizeof(ready_o));
			pl.set_response_status( TLM_OK_RESPONSE );
			msg(pl);
			break;
		
		/*-----------------------------------------------------------------------------*/
		default:
			cout << "ARPS bad address.\n";
			pl.set_response_status( TLM_ADDRESS_ERROR_RESPONSE );
			break;
		}
		break;
	}
	default:
		pl.set_response_status( TLM_COMMAND_ERROR_RESPONSE );
		SC_REPORT_ERROR("ARPS", "TLM bad command");
		break;
	}
	
	//msg(pl);
	offset += sc_time(10, SC_NS);
}
/**************************************************************************************************/
void ARPS::msg(const pl_t& pl)
{
	stringstream ss;
	ss << hex << pl.get_address();
	sc_uint<32> val = *((sc_uint<32>*)pl.get_data_ptr());
	string cmd  = pl.get_command() == TLM_READ_COMMAND ? "read  " : "write ";

	string regname;
	switch(pl.get_address())
	{
	case ARPS_START_I : regname = "START_I"; break;
	case ARPS_READY_O: regname = "READY_O"; break;
	
	default: regname = "no reg";
	}

	string msg = cmd + "val: " + to_string((int)val) + " adr: " + ss.str();
	msg += " " + regname;
	msg += " @ " + sc_time_stamp().to_string();

    SC_REPORT_INFO("ARPS", msg.c_str());
}
/**************************************************************************************************/
sc_dt::sc_uint<16> ARPS::costSAD(sc_dt::sc_uint<8> i_curr_in,
                                 sc_dt::sc_uint<8> j_curr_in,
                                 sc_dt::sc_uint<8> i_ref_in,
                                 sc_dt::sc_uint<8> j_ref_in)
{
	sc_uint<16> err=0;
	sc_uint<8> data_ref,data_curr;
	sc_uint<16> addr_ref,addr_curr;
	
	for(int i=0;i<MB_SIZE;i++){
		for(int j=0;j<MB_SIZE;j++){
            
			addr_curr=ROW_SIZE*(i+i_curr_in)+(j+j_curr_in);
			addr_ref=ROW_SIZE*(i+i_ref_in)+(j+j_ref_in);
            
			data_curr=addr_data_resize(addr_curr,'c');
			data_ref=addr_data_resize(addr_ref,'r');
            
            //data_curr=bram_curr[addr_curr];
			//data_ref=bram_ref[addr_ref];
			err=err+abs(sc_int <16>(data_curr-data_ref));
            cnt_arps_sad_block++;
		}
	}
	return err;
}
/**************************************************************************************************/
char ARPS::checkBounded(int xcol,int yrow,unsigned int c,unsigned int r)
{
	char tmp;
	if((yrow<0) || ((yrow+MB_SIZE)>=r) || (xcol<0) || ((xcol+MB_SIZE>=c)))
		tmp=1;
	else
		tmp=0;
	return tmp;
}
/**************************************************************************************************/
void ARPS::proc()
{
	unsigned int cost;
	unsigned int mbCount=0;
	unsigned int costs=65535;
	int SDSP_jx[5]={0,-1,0,1,0};
	int SDSP_iy[5]={-1,0,0,0,1};
	int LDSP_jx[6]={};
	int LDSP_iy[6]={};
	int point;
	int x,y;  
	int ref_bl_jx,ref_bl_iy;

	int stepSize;
	int maxIndex;
	char doneFlag;
	char vect_iy,vect_jx;
	
	//#ifdef QUANTUM
		tlm_utils::tlm_quantumkeeper qk;
		qk.reset();
  // #endif
	while(1)
	{
		if(start_i & 0x1)
		{
            mbCount=0;
			start_i=0;//treba proveriti
			qk.inc(period);
			qk.sync();
			
			for(int i=0;i<ROW_SIZE-MB_SIZE+1;i+=MB_SIZE){ 
				for(int j=0;j<COL_SIZE-MB_SIZE+1;j+=MB_SIZE){
                    cnt_arps_control_block++;
					qk.inc(period);
					qk.sync();
					x=j;
					y=i;
					costs=costSAD(i,j,i,j);//central point=2;
					if(j==0){
						stepSize=2;
						maxIndex=5;
						vect_iy=0;
						vect_jx=0;
					}
					else{
						stepSize=MAX2(abs(vect_iy),abs(vect_jx));
						if(((abs(vect_iy)==stepSize) && (abs(vect_jx)==0)) || (abs(vect_jx)==stepSize) && (abs(vect_iy)==0)){
							maxIndex=5;
						}
						else{
							maxIndex=6;
							LDSP_jx[5]=vect_jx;
							LDSP_iy[5]=vect_iy;
						}
					}
					//LARGE DIAMOND SEARCH PATTERN
					LDSP_jx[0]=0;         		LDSP_iy[0]=-stepSize;
					LDSP_jx[1]=-stepSize; 	LDSP_iy[1]=0;
					LDSP_jx[2]=0;         		LDSP_iy[2]=0;
					LDSP_jx[3]=stepSize;  	LDSP_iy[3]=0;
					LDSP_jx[4]=0;         		LDSP_iy[4]=stepSize;
      
					cost=costs;
					point=2;
      
					for(int k=0;k<maxIndex;k++){
             
						ref_bl_iy=y+LDSP_iy[k];//row
						ref_bl_jx=x+LDSP_jx[k];//col
						if(checkBounded(ref_bl_jx,ref_bl_iy,COL_SIZE,ROW_SIZE))
							continue;
						if((k==2)||(stepSize==0))
							continue;
						costs=costSAD(i,j,ref_bl_iy,ref_bl_jx);
						if(costs<cost){
							cost=costs;
							point=k;
						}
                        cnt_arps_control_block++;
					}
					x=x+LDSP_jx[point];
					y=y+LDSP_iy[point];
					costs=cost;
					doneFlag=0;
      
					while(doneFlag==0){
						point=2;
						for(int k=0;k<5;k++){
							ref_bl_iy=y+SDSP_iy[k];//row
							ref_bl_jx=x+SDSP_jx[k];//col
							if((checkBounded(ref_bl_jx,ref_bl_iy,COL_SIZE,ROW_SIZE))|| k==2){
								continue;
							}
							else if((ref_bl_jx<(j-P_SIZE))||(ref_bl_jx>(j+P_SIZE))||(ref_bl_iy<(i-P_SIZE))||(ref_bl_iy>(i+P_SIZE))){
								continue;
							}
							costs= costSAD(i,j,ref_bl_iy,ref_bl_jx);
							if(costs<cost){
								cost=costs;
								point=k;
							}
                            cnt_arps_control_block++;
						}
						doneFlag=1;
						if(point!=2){
							doneFlag=0;
							y=y+SDSP_iy[point];
							x=x+SDSP_jx[point];
							costs=cost;
						}
					}
					vect_jx=x-j;
					vect_iy=y-i;
					
					//std::cout<<"vect_jx="<<x-j<<std::endl<<"vect_iy="<<y-i<<std::endl;
                    
                    //Write to BRAM_MV results
                    write_bram(sc_dt::sc_uint<32>(mbCount),sc_dt::sc_int<32>(x-j),'m');
                    write_bram(sc_dt::sc_uint<32>(mbCount+1),sc_dt::sc_int<32>(y-i),'m');

					mbCount+=2;
					costs=65535;

				}//for
			}//for
			ready_o=1;
			p_out->write(SC_LOGIC_1);
			SC_REPORT_INFO("ARPS", string("Interrupt=1 @ " + sc_time_stamp().to_string()).c_str());
			qk.inc(period);
			qk.sync();
			p_out->write(SC_LOGIC_0);
			SC_REPORT_INFO("ARPS", string("Interrupt=0 @ " + sc_time_stamp().to_string()).c_str());
			qk.inc(period);
			qk.sync();
			
		}
		
		qk.inc(period);
		if (qk.need_sync())
		{
			qk.sync();
			//SC_REPORT_INFO("ARPS", string("Synced @ " + sc_time_stamp().to_string()).c_str());
		}
		
	}
    
}
sc_dt::sc_uint<32> ARPS::read_bram(sc_dt::sc_uint<32> addr, char socket)
{
    
    pl_t pl;
    sc_dt::sc_uint<32> data_bram;
    pl.set_address(addr);
    pl.set_data_length(1);
    pl.set_data_ptr((unsigned char*)&data_bram);
    pl.set_command( tlm::TLM_READ_COMMAND );
    pl.set_response_status ( tlm::TLM_OK_RESPONSE );
    sc_core::sc_time offset = sc_core::SC_ZERO_TIME;

    if (socket == 'r'){
        bram_ref_b->b_transport(pl, offset);
        cnt_byte_rec_b_ref+=4;//1? because we receive 4B but use 1B
        time_bram_ref+=PERIOD_T;
    }
    else if (socket == 'c'){
        bram_curr_b->b_transport(pl, offset);
        cnt_byte_rec_b_curr+=4;//or 1?
        time_bram_curr+=PERIOD_T;
    }
    //we don't use it for reading
    /*
    else if (socket == 'm'){
        bram_mv_b->b_transport(pl, offset);
        cnt_byte_rec_b_mv+=4;//or 1?
        
    }
    */
    else{
        SC_REPORT_ERROR("ARPS: Read Bram", "Wrong command.");
    }
    /*we receive 4 bytes, but we use only 1 byte, 
     * see method "addr_data_resize" 
     * if we count all 4 bytes, then we have to set 
     * cnt_byte_received += 4
     */
    //cnt_byte_received += 1;//
    
    return data_bram;
}

void ARPS::write_bram(sc_dt::sc_uint<32> addr, sc_dt::sc_int<32> data, char socket)
{
    pl_t pl;
    pl.set_address(addr*4);
    pl.set_data_length(1);
    pl.set_data_ptr((unsigned char*) & data);
    pl.set_command(tlm::TLM_WRITE_COMMAND);
    pl.set_response_status (tlm::TLM_OK_RESPONSE);
    /*
    if (socket == 'r'){
        bram_ref_b->b_transport(pl, offset);
        cnt_byte_sent_b_ref+=4;
        
    }
    else if (socket == 'c'){
        bram_curr_b->b_transport(pl, offset);
        cnt_byte_sent_b_curr+=4;
        
    }
    else*/ if (socket == 'm'){
        bram_mv_b->b_transport(pl, offset);
        cnt_byte_sent_b_mv+=4;
        time_bram_mv+=PERIOD_T;
        //msg(pl);
    }
    else{
        SC_REPORT_ERROR("ARPS: Write Bram", "Wrong command.");
    }
    /*
    4 bytes are written in BRAM_MV
     */
    //cnt_byte_sent += 4;
}

sc_dt::sc_uint<8> ARPS::addr_data_resize(sc_dt::sc_uint<16> addr_in,char socket){
    sc_dt::sc_uint<8> data_out8,sel;
    sc_dt::sc_uint<32> tmp,bram_addr,data_in32;
    bram_addr=((addr_in>>2)<<2);//65535/4=16383*4=65532 =>bram_address, in bram we have an array of 16384 32bit data
    data_in32=read_bram(bram_addr,socket);
    sel=addr_in-bram_addr;
    //select pixel value of 32 bit data
    //example: data_in32 = 0xFE01FF02FF => (254,1,255,2) , sel=0 data_in=254
    switch(sel){
        case 0:{
            data_out8=(data_in32>>24) & 0x000000FF;//shift_right for 24 bit to reach first 8 bit-s
        }break;
        case 1:{
            data_out8=(data_in32>>16) & 0x000000FF;
        }break;
        case 2:{
            data_out8=(data_in32>>8) & 0x000000FF;
        }break;
        case 3:{
            data_out8=data_in32 & 0x000000FF;
        }break;
        default:
            SC_REPORT_ERROR("ARPS: addr_data_resize", "Sel is not valid!");
    }
    return data_out8;
}
