#ifndef _VP_HPP_
#define _VP_HPP_

#include <systemc>
#include <sstream>
#include <iostream>
#include <tlm_utils/simple_target_socket.h>
#include <tlm_utils/simple_initiator_socket.h>

#include "interconnect.hpp"
#include "bram_ctrl.hpp"
#include "bram.hpp"
#include "arps.hpp"

typedef tlm::tlm_base_protocol_types::tlm_payload_type pl_t;

class VP :
	sc_core::sc_module
{
public:
	VP(sc_core::sc_module_name);

	tlm_utils::simple_target_socket<VP> s_cpu;//TARGET public socket (CPU=>VP)
	sc_core::sc_out<sc_dt::sc_logic> p_out0;//port for interrupt

protected:
	tlm_utils::simple_initiator_socket<VP> s_interconnect;//INITIATOR socket
	
    INTERCONNECT in;//Interconnect moduel
	
	ARPS ar;//ARPS module
	
	BRAM_CTRL b_ctrl_0;//bram_controller for BRAM_CURR, used only one port for writting
	BRAM_CTRL b_ctrl_1;//bram_controller for BRAM_REF, used only one port for writting
	BRAM_CTRL b_ctrl_2;//bram_controller for BRAM_MV, used only one port for reading
	
	BRAM b_curr;//Dual Port BRAM memory for CURR FRAME
	BRAM b_ref;//Dual Port BRAM memory for REF FRAME
	BRAM b_mv;//Dual Port BRAM memory for MV (Motion Vectors)
    
    void b_transport(pl_t&, sc_core::sc_time&);
};

#endif
