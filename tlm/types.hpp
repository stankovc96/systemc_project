#ifndef _VP_TSP_ADDR_H_
#define _VP_TSP_ADDR_H_
#include <sstream>

#define W_DATA 8
#define ROW_SIZE 256
#define COL_SIZE 256

#define MB_SIZE 16
#define P_SIZE 7

#define DEPTH_CURR (ROW_SIZE*COL_SIZE) //65536
#define DEPTH_REF (ROW_SIZE*COL_SIZE) //65536
#define DEPTH_MV (2*(ROW_SIZE*COL_SIZE)/(MB_SIZE*MB_SIZE)) //512

#define MAX2(x, y) (((x) > (y)) ? (x) : (y))


#define BRAM_CTRL_0_ADDR_RANGE 64 //64KB -> 16384 locations, 32 bit data width, 4 pix on one location
#define BRAM_CTRL_1_ADDR_RANGE 64 //64KB -> 16384 locations, 32 bit data width, 4 pix on one location
#define BRAM_CTRL_2_ADDR_RANGE 4 // 4KB -> used only 512 locations, minimal BRAM size is 4KB for 32 bit data width


typedef tlm::tlm_base_protocol_types::tlm_payload_type pl_t;

//AXI_LITE reg
const sc_dt::uint64 ARPS_START_I = 0;
const sc_dt::uint64 ARPS_READY_O = 4;

const sc_dt::uint64 VP_ADDR_AXI_L_ARPS  = 0x43C00000;//AXI_LITE ->start,ready
const sc_dt::uint64 VP_ADDR_BRAM_CTRL_0 = 0x40000000;//BRAM_CURR
const sc_dt::uint64 VP_ADDR_BRAM_CTRL_1 = 0x42000000;//BRAM_REF
const sc_dt::uint64 VP_ADDR_BRAM_CTRL_2 = 0x44000000;//BRAM_MV

//AXI_LITE addr reg
const sc_dt::uint64 VP_ADDR_AXI_L_ARPS_START_I = VP_ADDR_AXI_L_ARPS + ARPS_START_I;//start address
const sc_dt::uint64 VP_ADDR_AXI_L_ARPS_READY_O = VP_ADDR_AXI_L_ARPS + ARPS_READY_O;//ready address

//LOW_ADDR
const sc_dt::uint64 VP_ADDR_AXI_L_ARPS_L  = VP_ADDR_AXI_L_ARPS;
const sc_dt::uint64 VP_ADDR_BRAM_CTRL_0_L = VP_ADDR_BRAM_CTRL_0;
const sc_dt::uint64 VP_ADDR_BRAM_CTRL_1_L = VP_ADDR_BRAM_CTRL_1;
const sc_dt::uint64 VP_ADDR_BRAM_CTRL_2_L = VP_ADDR_BRAM_CTRL_2;

//HIGH_ADDR
const sc_dt::uint64 VP_ADDR_AXI_L_ARPS_H  = VP_ADDR_AXI_L_ARPS + 4;
const sc_dt::uint64 VP_ADDR_BRAM_CTRL_0_H = VP_ADDR_BRAM_CTRL_0 + BRAM_CTRL_0_ADDR_RANGE*1024;
const sc_dt::uint64 VP_ADDR_BRAM_CTRL_1_H = VP_ADDR_BRAM_CTRL_1 + BRAM_CTRL_1_ADDR_RANGE*1024;
const sc_dt::uint64 VP_ADDR_BRAM_CTRL_2_H = VP_ADDR_BRAM_CTRL_2 + BRAM_CTRL_2_ADDR_RANGE*1024;

#endif
