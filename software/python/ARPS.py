try:
	import cv2 as cv
	import numpy as np
	import math
except ImportError:
	print ("library not installed")
#global

Min=65535	#256x256 16x16x2^8
mbSize=16	#16x16 za "chest" 8
p=7	#za "chest" 5, najbolje staviti 7

zero=''
frameLimit=3207
folder='/home/stefan/Desktop/bm_arps/spec/img/golf'
filename='sample'
ext='.jpg'
#---------------------------------------------------------
def main():
	img0=folder+filename+'1'+ext
	imgI=cv.imread(img0,0)#prethodni frejm(referentni)
	cv.imshow('ARPS',imgI)
	cnt=1
	row, col= imgI.shape
	print('Video name: "'+str(filename)+str(ext)+'"')
	print('Number of frames:'+str(frameLimit)) 
	print('Video size:'+str(row)+'x'+str(col))
	print('Block size:'+str(mbSize)+'x'+str(mbSize)+', P='+str(p))
	
	print("start")
	print("frame1")
	while(True):
		#400x512
		img0=folder+filename+zero+str(int(cnt))+ext
		cnt=cnt+1
		img1=folder+filename+zero+str(int(cnt))+ext

		print('frame'+str(cnt))
		imgI=cv.imread(img0,0)#previous frame(referent)
		imgP=cv.imread(img1,0)#current frame
		if(cnt>=frameLimit):
			cnt=0
		
		brX=-1
		brY=-1
		vec,comp=_ARPS(imgP,imgI,mbSize,p)
		#print(vec)
		print('Comput='+str(comp))
		for i in range(0,row-mbSize+1,mbSize):
			brY=brY+1#h
			brX=-1
			for j in range(0,col-mbSize+1,mbSize):
				brX=brX+1#w
				#print("Y"+str(brY)+" X"+str(brX))
				x=j+vec[brY,brX,0]
				y=i+vec[brY,brX,1]
				
				#if(math.fabs(x-j)>2 or math.fabs(y-i)>2):
				cv.arrowedLine(imgP,(int(x),int(y)),(int(j),int(i)), (255,0,0), 1,cv.LINE_AA,0,0.2)
				#cv.line(imgP,(int(j),int(i)),(int(x),int(y)),(255,0,0),1)
			
		cv.imshow('ARPS',imgP)
		#imgComp=motionComp(imgI,vec,mbSize)
		#TSSpsnr=imgPSNR(imgP,imgComp,255)
		#print('PSNR='+str(round(TSSpsnr,2)))
		if cv.waitKey(10) & 0xFF == ord('q'):
			break

	cv.destroyAllWindows()

def _costMAD(block1, block2):
	block1 = block1.astype(np.float32)
	block2 = block2.astype(np.float32)
	return np.mean(np.abs(block1 - block2))

def _minCost(costs):
	h, w = costs.shape
	mi = costs[np.int((h-1)/2), np.int((w-1)/2)]
	dy = np.int((h-1)/2)
	dx = np.int((w-1)/2)
    #mi = 65535
    #dy = 0
    #dx = 0

	for i in range(h): 
		for j in range(w): 
			if costs[i, j] < mi:
				mi = costs[i, j]
				dy = i
				dx = j

	return dx, dy, mi

def _checkBounded(xval, yval, w, h, mbSize):
	if ((yval < 0) or(yval + mbSize >= h) or(xval < 0) or(xval + mbSize >= w)):
		return False
	else:
		return True
def _ARPS(imgP, imgI, mbSize, p):
    # Computes motion vectors using Adaptive Rood Pattern Search method
    #
    # Input
    #   imgP : The image for which we want to find motion vectors
    #   imgI : The reference image
    #   mbSize : Size of the macroblock
    #   p : Search parameter  (read literature to find what this means)
    #
    # Ouput
    #   motionVect : the motion vectors for each integral macroblock in imgP
    #   ARPScomputations: The average number of points searched for a macroblock

	h, w = imgP.shape

	vectors = np.zeros((np.int(h / mbSize), np.int(w / mbSize), 2))
	costs = np.ones((6))*65537

	SDSP = []
	SDSP.append([0, -1])
	SDSP.append([-1, 0])
	SDSP.append([0, 0])
	SDSP.append([1, 0])
	SDSP.append([0, 1])

	LDSP = {}

	checkMatrix = np.zeros((2 * p + 1, 2 *p+1))

	computations = 0

	for i in range(0, h - mbSize + 1, mbSize):
		for j in range(0, w - mbSize + 1, mbSize):
			x = j
			y = i
			costs[2] = _costMAD(imgP[i:i + mbSize, j:j + mbSize], imgI[i:i + mbSize, j:j + mbSize])

			checkMatrix[p, p] = 1
			computations += 1

			if (j == 0):
				stepSize = 2
				maxIndex = 5
			else:
				u = vectors[np.int(i / mbSize), np.int(j / mbSize) - 1, 0]
				v = vectors[np.int(i / mbSize), np.int(j / mbSize) - 1, 1]
				stepSize = np.int(np.max((np.abs(u), np.abs(v))))

				if (((np.abs(u) == stepSize) and (np.abs(v) == 0)) or((np.abs(v) == stepSize) and (np.abs(u) == 0))):
					maxIndex = 5
				else:
					maxIndex = 6
					LDSP[5] = [np.int(v), np.int(u)]

            # large diamond search
			LDSP[0] = [0, -stepSize]
			LDSP[1] = [-stepSize, 0]
			LDSP[2] = [0, 0]
			LDSP[3] = [stepSize, 0]
			LDSP[4] = [0, stepSize]

			for k in range(maxIndex):
				refBlkVer = y + LDSP[k][1]
				refBlkHor = x + LDSP[k][0]

				if not _checkBounded(refBlkHor, refBlkVer, w, h, mbSize):
					continue

				if ((k == 2) or (stepSize == 0)):
					continue

				costs[k] = _costMAD(imgP[i:i + mbSize, j:j + mbSize], imgI[refBlkVer:refBlkVer + mbSize, refBlkHor:refBlkHor + mbSize])
				computations += 1
				checkMatrix[LDSP[k][1] + p, LDSP[k][0] + p] = 1

			if costs[2] != 0:
				point = np.argmin(costs)
				cost = costs[point]
			else:
				point = 2
				cost = costs[point]

			x += LDSP[point][0]
			y += LDSP[point][1]
			costs[:] = 65537
			costs[2] = cost

			doneFlag = 0
			while (doneFlag == 0):
				for k in range(5):
					refBlkVer = y + SDSP[k][1]
					refBlkHor = x + SDSP[k][0]

					if not _checkBounded(refBlkHor, refBlkVer, w, h, mbSize):
						continue

					if k == 2:
						continue
					elif ((refBlkHor < j - p) or (refBlkHor > j + p) or(refBlkVer < i - p) or(refBlkVer > i + p)):
						continue
					elif (checkMatrix[y - i + SDSP[k][1] + p, x - j + SDSP[k][0] + p] == 1):
						continue

					costs[k] = _costMAD(imgP[i:i + mbSize, j:j + mbSize], imgI[refBlkVer:refBlkVer + mbSize, refBlkHor:refBlkHor + mbSize])
					checkMatrix[y-i+SDSP[k][1]+p,x-j+SDSP[k][0]+p] = 1
					computations += 1

				if costs[2] != 0:
					point = np.argmin(costs)
					cost = costs[point]
				else:
					point = 2
					cost = costs[point]

				doneFlag = 1
				if point != 2:
					doneFlag = 0
					y += SDSP[point][1]
					x += SDSP[point][0]
					costs[:] = 65537
					costs[2] = cost
			#print("i"+str(i/mbSize)+" j"+str(j/mbSize))
			vectors[np.int(i/mbSize),np.int(j / mbSize), :] = [x - j, y - i]

			costs[:] = 65537

			checkMatrix[:, :] = 0

	return vectors, computations / ((h * w) / mbSize**2)
main()
