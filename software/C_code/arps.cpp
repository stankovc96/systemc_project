/*
Adaptive rood pattern search
g++ main.cpp -o main `pkg-config --cflags --libs opencv4`
*/
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <assert.h>
#include <fstream>

#define MAX2(x, y) (((x) > (y)) ? (x) : (y))

#define showFPS

#define folder "gray"
#define location "gray/sample"

#define extension ".jpg"
#define mbSize 16  /*Macro Block Size 16x16 */
#define p 7
#define FRAMELIMIT 6



void destroyArray(int **arr);

char checkBounded(int,int,unsigned int ,unsigned int);
void motionARPS(cv::Mat,cv::Mat ,unsigned int,unsigned int ,int **);
float imgPSNR(cv::Mat,cv::Mat);
cv::Mat motionComp(cv::Mat,int **);

unsigned int costSAD(cv::Mat ,cv::Mat , int ,int , int , int );

//********************************MAIN***********************************
int main( int argc, char** argv ) {
	std::string fold=folder;
  std::cout<<"Start\n";
	cv::Mat currentImg,refImg;
	cv::Point ps,pe;
	cv::Scalar S;
	std::string name = location;
	std::string ext = extension;
	std::string filename;
	std::string firstFrame=location+std::to_string(0)+extension;
	
	refImg = cv::imread(firstFrame,cv::IMREAD_GRAYSCALE);
  if(! refImg.data ) {
		std::cout <<  "Frame not found" << std::endl ;
		return -1;
  }
  cv::namedWindow("ARPS",cv::WINDOW_AUTOSIZE);
  cv::imshow("ARPS",refImg);
	
	unsigned int row=refImg.rows;
	unsigned int col=refImg.cols;
	unsigned int frameNUM=0,br=0,x=0,y=0;
	unsigned int frame=0;
	
	
	filename=name+std::to_string(frameNUM)+ext;
	std::cout<<"row x col: "<<row<<"x"<<col<<std::endl;
	std::cout<<"Block size: "<<mbSize<<"\n";
	std::cout<<"P: "<<p<<"\n";
	std::cout<<filename<<"\n";
	
#ifdef showFPS
	time_t start, end;
	time(&start);
	double fps;
#endif
	
//---------------------------WHILE-------------------------------
  while(true){
  	
  	int **vec;

  	vec=new int*[2];
		for (int h = 0; h < 2; h++){
			vec[h] = new int[(row*col)/(mbSize*mbSize)];
			for (int w = 0; w < (row*col)/(mbSize*mbSize); w++){
				vec[h][w] =0;
    	}
  	}
  	
		#ifdef showFPS
            time(&end);
            frame++;
            if(difftime(end,start)>=1){
                std::cout<<"fps="<<(float)(frame/(difftime(end,start)))<<"\n";
                time(&start);
                frame=0;
            }
		#endif
		
    refImg = cv::imread(filename,cv::IMREAD_GRAYSCALE);
  	frameNUM=frameNUM+1;
  	if(frameNUM>=FRAMELIMIT){
  		frameNUM=0;
  		//break;
  	}
  	filename=name+std::to_string(frameNUM)+ext;
  	currentImg=cv::imread(filename,cv::IMREAD_GRAYSCALE);
  	
  	if(!refImg.data || !currentImg.data) {
			std::cout <<  "Could not open or find the image" << std::endl ;
			return -1;
  	}
  	  	
   
  	motionARPS(currentImg,refImg,row,col,vec);
  	assert (vec!=NULL);
  
		int br=0;
  	for(unsigned int i=0;i<(row-mbSize+1);i+=mbSize){//row
        for(unsigned int j=0;j<(col-mbSize+1);j+=mbSize){//col
				  
				x=j+vec[1][br];//col
				y=i+vec[0][br];//row
			    br++;
				cv::Point pe(j,i);//x axis and y oxis
				cv::Point ps(x,y);

				cv::arrowedLine(currentImg,ps,pe,S(255),1,CV_8U,0,0.2);

  		}
    }
    
    cv::imshow("ARPS",currentImg);
    destroyArray(vec);
    if(cv::waitKey(1)==27)
        exit(0);
  }//end of while
  //---------------------------END OF WHILE---------------------------------
  return 0;
}

unsigned int costSAD(cv::Mat currentImage,cv::Mat refImage,int bl_st_curr_i,int bl_st_curr_j,int bl_st_ref_i,int bl_st_ref_j)
{ 
  unsigned int err=0;  

  for(int i=0;i<mbSize;i++){
    for(int j=0;j<mbSize;j++)
    {
      err=err+(abs(currentImage.at<uchar>(i+bl_st_curr_i, j+bl_st_curr_j)-refImage.at<uchar>(i+bl_st_ref_i, j+bl_st_ref_j)));
    }
  }

  return err;
}


void motionARPS(cv::Mat currImage,cv::Mat refImage,unsigned int row,unsigned int col,int **vectors)
{  

  unsigned int cost;
  unsigned int mbCount=0;
  unsigned int costs=65535;
  /*
  int SDSP[5][2]={{ 0,-1},
 					{-1, 0},
 					{ 0, 0},
 					{ 1, 0},
 					{ 0, 1}};//
 	//SMALL DIAMOND SEARCH PATTERN
  */
  int SDSP_jx[5]={0,-1,0,1,0};
  int SDSP_iy[5]={-1,0,0,0,1};
  //int LDSP[6][2]={};
  int LDSP_jx[6]={};
  int LDSP_iy[6]={};
  
  int point;
  int x,y;  
  int ref_bl_jx,ref_bl_iy;

  int stepSize;
  int maxIndex;
  int u,v;
  char doneFlag;
  int vect_iy,vect_jx;

	
//---------------------------------------
  for(int i=0;i<row-mbSize+1;i+=mbSize){ 
   for(int j=0;j<col-mbSize+1;j+=mbSize){
      x=j;
      y=i;
      
      costs=costSAD(currImage,refImage,i,j,i,j);//central point=2;
      
      if(j==0){
      	stepSize=2;
      	maxIndex=5;
      	vect_iy=0;
      	vect_jx=0;

      }
      else{
      	//u=vectors[0][mbCount-1];
      	//v=vectors[1][mbCount-1];
      	//u=vect_iy;
      	//v=vect_jx;
      	
      	stepSize=MAX2(abs(vect_iy),abs(vect_jx));
          	
      	if(((abs(vect_iy)==stepSize) && (abs(vect_jx)==0)) || (abs(vect_jx)==stepSize) && (abs(vect_iy)==0)){
      		maxIndex=5;
      	}
      	else{
      		maxIndex=6;
      		LDSP_jx[5]=vect_jx;
      		LDSP_iy[5]=vect_iy;
      		
      	}
      }
      //LARGE DIAMOND SEARCH PATTERN
      LDSP_jx[0]=0;         LDSP_iy[0]=-stepSize;
      LDSP_jx[1]=-stepSize; LDSP_iy[1]=0;
      LDSP_jx[2]=0;         LDSP_iy[2]=0;
      LDSP_jx[3]=stepSize;  LDSP_iy[3]=0;
      LDSP_jx[4]=0;         LDSP_iy[4]=stepSize;
      
      
	    
      cost=costs;
      point=2;
      
	  for(int k=0;k<maxIndex;k++){
      	ref_bl_iy=y+LDSP_iy[k];//row
      	ref_bl_jx=x+LDSP_jx[k];//col
      	if(checkBounded(ref_bl_jx,ref_bl_iy,col,row))
      		continue;
      	if((k==2)||(stepSize==0))
      		continue;
      	costs=costSAD(currImage,refImage,i,j,ref_bl_iy,ref_bl_jx);
        if(costs<cost){
           cost=costs;
           point=k;
        }
      }
       
     
      x=x+LDSP_jx[point];
      y=y+LDSP_iy[point];
      
      costs=cost;
      doneFlag=0;
      
      while(doneFlag==0){
        point=2;
        for(int k=0;k<5;k++){
      		ref_bl_iy=y+SDSP_iy[k];//row
      		ref_bl_jx=x+SDSP_jx[k];//col
      		if((checkBounded(ref_bl_jx,ref_bl_iy,col,row))|| k==2){
      			continue;
      		}
      		
      		else if((ref_bl_jx<(j-p))||(ref_bl_jx>(j+p))||(ref_bl_iy<(i-p))||(ref_bl_iy>(i+p))){
      			continue;
            }
      		costs= costSAD(currImage,refImage,i,j,ref_bl_iy,ref_bl_jx);
      		if(costs<cost){
      		    cost=costs;
      		    point=k;
      		}
      		
      		
      }
     	doneFlag=1;
     	
        if(point!=2){
     		doneFlag=0;
     		y=y+SDSP_iy[point];
     		x=x+SDSP_jx[point];
     	
     		costs=cost;
     	}
     }
     
     vectors[1][mbCount]=x-j;
     vectors[0][mbCount]=y-i;
     vect_jx=x-j;
     vect_iy=y-i;

	 mbCount+=1;
     costs=65535;

		}//for
	}//for

  
    
}

char checkBounded(int xcol,int yrow,unsigned int c,unsigned int r)
{
	char tmp;
	if((yrow<0) || ((yrow+mbSize)>=r) || (xcol<0) || ((xcol+mbSize>=c)))
		tmp=1;
	else
		tmp=0;
	return tmp;
}


void destroyArray(int **arr)
{
    free(*arr);
    free(arr);
}
//*******************************************************

//******************************************************************
cv::Mat motionComp(cv::Mat imgI,int **vec)
{
	unsigned int row=imgI.rows;
	unsigned int col=imgI.cols;
	int mbCount=0;
	cv::Scalar s;
	cv::Mat imageComp;
	imageComp =cv::Mat::zeros(row,col, CV_8U);
	


	int dx,dy;
	int rr,cc;
	
	for (int i=0;i<(row-mbSize+1);i+=mbSize){
		for (int j=0;j<(col-mbSize+1);j+=mbSize){ 
			dx=j+vec[1][mbCount];//col
			dy=i+vec[0][mbCount];//row
			mbCount++;
			rr=i-1;
			cc=j-1;
			for(int r=dy;r<dy+mbSize;r++){
				rr++;
				cc=j-1;
				for(int c=dx;c<dx+mbSize;c++){
					cc++;
					if(r<0 || c<0 || r>=row || c>=col)
						continue;
					imageComp.at<uchar>(rr,cc) = imgI.at<uchar>(r,c);
				}
			}
		}
	}
	return imageComp;
}

float imgPSNR(cv::Mat imgP,cv::Mat imgComp){
	int row=imgP.rows;
	int col=imgP.cols;
	int n=mbSize*mbSize;
	float err=0,mse=0,psnr=0;
	for (int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			err=err+pow((imgP.at<uchar>(i,j)-imgComp.at<uchar>(i,j)),2);
		}
	}
	mse=err/(row*col);
	psnr=10*log10((n*n)/mse);
	
	return psnr;
}
